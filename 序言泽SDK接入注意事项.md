# 序言泽广告sdk接入事项汇总
```
温馨提示：接入过程中，优先阅读此文档。接入顺序：先接入核心sdk，然后按需添加三方sdk（参考序言泽三方sdk接入文档），如果需要接保活和场景，请参考序言泽保活sdk接入文档和序言泽场景广告接入文档，接入信息流自渲染广告请参考序言泽广告渲染框架文档
```
## 1. 序言泽广告sdk对外暴露接口
   * IUrlsProvider接口（可选）
   * IDefaultConfigProvider （可选）
   * 是否连接测试服-setTestServe（可选，默认为false）
   * 客户端传参-setCustomParams（必选）
     * 内部   
       * accid
       * muid
       * appQid
       * cleanAppQid
       * isTourist
       * appSmallver
       * appSmallverInt
       * softName
       * softType
       * userflag   天气项目，用户分层
       * userinfo  用户信息
       * lowGps    天气项目要求，只使用network定位
       * canUseMacAddress  sdk能否使用mac地址
       * isUseCacheFirst  控制场景广告是否优先使用缓存
       * cacheTimeForSafeExposure   预曝光缓存时间（单位ms）
       * appTypeId  app标识，在新萌sdk平台获取
       * aaid
       * oaid
       * useClientLocation  是否由客户端传递lat, lng, lbsTime位置信息，传的话，sdk里面不再请求
       * lat
       * lng
       * lbsTime 获取 lat、lng的系统时间
     * 外部
       * appTypeId
       * aaid
       * oaid
       * canUseMacAddress  sdk能否使用mac地址
   * 请求广告时需要传参
     * pgtype
     * useCacheFirst
     * dialogstyle
     * except
     * pagenum
     * idx
     * gametype
     * vivostyle
   * 展现广告是需要参数
     * context或dialog 聚合广告需要
     * displayOrder 当一个位置即可能返回大图有可能返回小图时，改如何展现
     * 大图广告的圆角弧度
     * scaleType 大图拉伸的策略
     * labelStyle 使用圆角label还是方角label

## 2. 三方sdk的配置放在客户端,客户端要做以下工作

   * 导入三方sdk
   * 添加对应sdk的混淆规则（包括代码混淆和资源混淆）
   * 添加对应sdk的manifest文件配置 
* 中台里会根据导入sdk，初始化对应的sdk，客户端无需调用三方sdk的初始化方法



详见：序言泽三方sdk接入文档.md

## 3. 初始化序言泽核心sdk(必选)
   * 导入序言泽核心sdk
   * 添加混淆规则
   * 添加manifet配置
   * 调用初始化方法 
## 4. 保活功能（可选）

  目前提供两种保活方式，以下

   * 自研保活
   * 安天保活



​    详见： 序言泽保活sdk接入文档.md

## 5. 场景广告功能（可选）

​      详见：序言泽场景广告接入文档.md