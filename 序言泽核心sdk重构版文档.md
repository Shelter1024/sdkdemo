# 序言泽广告SDK接入文档
<span id = "catalog"></span>

## 目录
<ol>
<li><a href="#catalog">目录</a></li>
<li><a href="#embed-code">接入代码</a>
    <ul>
    <li><a href="#app-type-id">申请应用appTypeId</a> </li>
    <li><a href="#import-xyz-sdk">导入序言泽核心sdk</a> </li>
    <li><a href="#import-third-sdk">导入上游广告sdk</a> </li>
    <li><a href="#configuration-of-manifest">AndroidManifest配置</a>
        <ul>
            <li><a href="#add-permissions">添加权限</a></li>
            <li><a href="#add-hezan-providers">添加禾赞广告Provider</a></li>
            <li><a href="#configuration-of-runtime">运行环境配置</a></li>
        </ul>
    </li>
    <li><a href="#proguard">混淆配置</a> 
        <ul>
            <li><a href="#proguard-code">代码混淆</a></li>
            <li><a href="#proguard-resources">资源混淆</a></li>
        </ul>
    </li>
    </ul>
</li>
<li><a href="#init">初始化</a>
    <ul>
        <li><a href="#init-steps">初始化的两个阶段</a></li>
        <li><a href="#init-code">初始化代码</a>
            <ul>
                <li><a href="#init-custom-params">ICustomParams</a></li>
                <li><a href="#init-config-provider">IDefaultSlotConfigProvider</a></li>
                <li><a href="#defination-of-slot-type">slotType的定义</a></li>
                <li><a href="#init-custom-slot-config">CustomSlotConfig</a></li>
                <li><a href="#defination-of-platform">广告平台的定义</a></li>
                <li><a href="#defination-of-category">categoryType的定义</a></li>
                <li><a href="#init-links-provider">ILinksProvider</a></li>
                <li><a href="#init-xyz-ad-config">XYZAdConfig的构建</a></li>
            </ul>
        </li>
    </ul>
</li>
<li><a href="#load">加载广告</a>
    <ul>
        <li><a href="#load-related-classes">加载广告用到的类</a>
            <ul>
                <li><a href="#load-mediation-manager">IMediationManager</a></li>
                <li><a href="#load-scene-info">SceneInfo</a></li>
                <li><a href="#load-mediation-listener">MediationAdListener</a></li>
            </ul>
        </li>
        <li><a href="#load-splash">加载开屏广告</a>
            <ul>
                <li><a href="#load-splash-code">代码示例</a></li>
                <li><a href="#load-splash-manager">ISplashManager</a></li>
                <li><a href="#load-splash-callback">ISplashCallback</a></li>
            </ul>
        </li>
        <li><a href="#load-feed">加载feed广告（XYZFeedAd）</a>
            <ul>
                <li><a href="#load-feed-code">代码示例</a></li>
                <li><a href="#load-xyz-feed-ad">XYZFeedAd</a></li>
            </ul>
        </li>
        <li><a href="#load-reward-video">加载激励视频广告（XYZRewardVideoAd）</a>
            <ul>
                <li><a href="#load-reward-video-code">代码示例</a></li>
                <li><a href="#load-xyz-reward-video-ad">XYZRewardVideoAd</a></li>
                <li><a href="#load-reward-video-listener">IRewardVideoListener</a></li>
            </ul>
        </li>
        <li><a href="#load-interstitial">加载插屏广告(XYZInterstitialAd)</a>
            <ul>
                <li><a href="#load-interstitial-code">代码示例</a></li>
                <li><a href="#load-xyz-interstitial-ad">XYZInterstitialAd</a></li>
                <li><a href="#load-interstitial-listener">IInterstitialListener</a></li>
            </ul>
        </li>
        <li><a href="#load-banner">加载Banner广告（XYZBannerAd）</a>
            <ul>
                <li><a href="#load-banner-code">代码示例</a></li>
                <li><a href="#load-xyz-banner-ad">XYZBannerAd</a></li>
                <li><a href="#load-material-interaction-listener">IMaterialInteractionListener</a></li>
            </ul>
        </li>
    </ul>
</li>
<li><a href="#catalog">SDK错误码</a></li>
<li><a href="#catalog">注意事项</a></li>
</ol>

<span id = "embed-code"></span>

## 1.更新记录

|中台版本|修改日期|修改日志|修改人|
|:-|:-|:-|:-|
|1.3.120|2021.08.27|初版|谢添|

## 2. 接入代码

<span id = "app-type-id"></span>

### 2.1 申请应用appTypeId

请联系商务在序言泽广告平台创建应用appTypeId

<span id = "import-xyz-sdk"></span>

### 2.2 导入序言泽核心sdk

将adv_core-priv-release.aar复制到libs文件夹(没有的话须手动创建), 并将以下代码添加到您app的build.gradle中：
```gradle
depedencies { 
    api(name: 'adv_core-priv-release', ext: 'aar')
}
```

<span id = "import-third-sdk"></span>

（非必选）项目中默认已经接入禾赞广告，为了获取禾赞激励视频更好的播放体验，建议集成 ijkplayer-release.aar (由中台广告提供)，否者使用内置播放器
如果集成ijkplayer-release.aar，请添加如下混淆规则：
-keep class com.ijk.ijkplayer.IjkVideoView{*;}

```gradle
depedencies { 
    api(name: 'ijkplayer', ext: 'aar')
}
```

### 2.3 导入上游广告sdk
按需导入上游广告sdk。以下为当前序言泽广告sdk，支持的上游广告sdk版本。<br/>
<font color="#f33">请务必接入版本正确的上游广告sdk!!!</font>
|上游广告sdk|版本|
|:-|:-|
|穿山甲|3809|
|广点通| 4.390.1260          |
|百度| 9.15               |
|聚合| 111004_202108241533 |
|快手| 3.3.20.2            |
|游可赢| 2.2.0.21            |

<span id = "configuration-of-manifest"></span>

详见   序言泽三方sdk接入文档.md

### 2.4 AndroidManifest配置

<span id = "add-permissions"></span>

#### 2.4.1 添加权限  <font color="#ff3">开发任务</font>
为了有效覆盖目标用户，提升广告收益，请尽可能满足以下权限。
|权限|是否必须|用途|
|:-|:-|:-|
|android.permission.INTERNET|是|联网|
|android.permission.READ_PHONE_STATE|否|读取imei|
|android.permission.ACCESS_NETWORK_STATE|否|读取mac地址，判断联网状态|
|android.permission.WRITE_EXTERNAL_STORAGE|否|下载apk存储到sdcard|
|android.permission.ACCESS_WIFI_STATE|否|判断联网状态|
|android.permission.ACCESS_COARSE_LOCATION|否|定位，用于定向广告投放|
|android.permission.ACCESS_FINE_LOCATION|否|定位，用于定向广告投放|
|android.permission.WAKE_LOCK|否|避免播放激励视频时息屏|
|android.permission.REQUEST_INSTALL_PACKAGES|否|安装apk|
|android.permission.GET_TASKS|否|读取应用列表，用于广告精准投放|

<span id = "add-hezan-providers"></span>

#### 2.4.2 添加禾赞广告Provider<font color="#ff3">开发任务</font>
```xml
<provider
    android:name="com.luck.bbb.LuckFileProvider"
    android:authorities="${applicationId}.LuckFileProvider"
    android:exported="false"
    android:grantUriPermissions="true">
    <meta-data
        android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/luck_file_path" />
</provider>
```

<span id = "configuration-of-runtime"></span>

#### 2.4.3 运行环境配置
本SDK可运行于Android4.1 (API Level 16) 及以上版本。

<span id = "proguard"></span>

### 2.5 混淆配置

<span id = "proguard-code"></span>

#### 2.5.1 代码混淆

```
-keep class com.coke.**{*;}
-keep class com.wss.bbb.e.utils.SystemMarker{*;}

#EventBus start
-keepattributes *Annotation*
-keep class com.wss.bbb.e.eventbus.Subscribe{*;}
-keepclassmembers class * {
    @com.wss.bbb.e.eventbus.Subscribe <methods>;
}
-keep class com.wss.bbb.e.eventbus.ThreadMode { *; }
#EventBus end
#点击优化 start
-keep class com.wss.bbb.e.mediation.optimize.IAdUtils{*;}
-keep class com.wss.bbb.e.mediation.source.Material{
    public void updateCeffect(***);
    public *** getAdv(***);
}
-keep class com.wss.bbb.e.mediation.optimize.OptimizeStrategy{*;}
#点击优化 end
```


<span id = "proguard-resources"></span>

如果项目中导入ijkplayer.aar，需要额外添加混淆:

```
-keep class tv.danmaku.ijk.media.player.** {*; }
-keepclasseswithmembernames class tv.danmaku.ijk.media.player.IjkMediaPlayer{
public <fields>;
public <methods>;
}
-keepclasseswithmembernames class tv.danmaku.ijk.media.player.ffmpeg.FFmpegApi{
public <fields>;
public <methods>;
}
```

#### 2.5.2 资源混淆
资源混淆白名单，参见三方sdk接入文档

<span id = "init"></span>

## 3. 初始化

<font color="#f33">本sdk尚不支持多进程，只能在主进程初始化，且不能在子进程中调用广告代码。</font><br/>

<span id = "init-steps"></span>

### 3.1 初始化的两个阶段
为了满足工信部等主管部门的合规要求，本sdk的初始化拆分为两个阶段。两个阶段调用时机不同，请开发者务必注意。
|初始化方法|调用时机|作用|
|:-|:-|:-|
|WSSAdSdk#preInit(Context)|Application#onCreate()|预初始化|
|WSSAdSdk#init(Context)|preInit之后、主线程、用户同意隐私协议之后|主sdk初始化|

<span id = "init-code"></span>

### 3.2 初始化代码
```
ICusParams customParams = new ClientCustomParams();
IDefaultConfigProvider configProvider = new ClientDefaultConfigProvider();
ILinksProvider linksProvider = new ClientLinksProvider();
WSSAdConfig wssAdConfig = new WSSAdConfig.Builder()
        .setDefaultConfigProvider(configProvider)
        .setCustomParams(customParams)
        .setLinksProvider(linksProvider)
        .setTestServer(false)
        .setDebug(false)
        .build();
WSSAdSdk.init(app, wssAdConfig);
```
以下为相关类的说明。

<span id = "init-related-classes"></span>

#### 3.2.1 ICusParams
客户端需要实现ICusParams接口，sdk的部分能力需要依赖客户端。
|方法|内or外|说明|
|:-|:-|:-|
|accId|内|返回用户的accId|
|muid|内|返回用户的muid|
|appTypeId|内+外|app标识，在序言泽sdk平台获取|
|appQid|内|返回渠道id，如vivo20200525|
|cleanAppQid|内|返回干净的渠道id，如vivo|
|isTourist|内|返回是否是游客，是:"1" 否:"0"|
|appSmallVerInt|内|小版本|
|appSmallVer|内|小版本|
|aaid|内+外|android10以后设备标识|
|oaid|内+外|android10以后设备标识aaid|
|userflag|内|简单天气内部使用，用户分层|
|userinfo|内|（公共参数）用户信息。如：* {"sex":"性别：1-男；2-女；0-未知", "bd":"生日，格式：1990-05-06","regts":"注册时间，注册信息返回，1579509208", "lastinstall":"最近一次安装时间(单位秒)","usertype":"账号类型/三方账号类型：0-游客；1-手机；2-微信；3-QQ；4-vivo；5-微博；6-华为"}（没有用到传null）。sex/bd/regts/lastinstall/usertype为userinfo的常驻字段，各项目可按需向userinfo中添加用户相关属性，比如：刺客传说可以传入用户等级level，新增属性在userinfo中与常驻属性同一层级|
|lowGps|内|别项目如天气，日历考虑到耗电因素，只使用network定位|
|canUseMacAddress|内| 兼容华为手机过审策略|
|isUseCacheFirst|内|控制广告是否先使用缓存|
|cacheTimeForSafeExposure|内|预曝光缓存时间 单位ms|
|useClientLocation|内|是否由客户端传递lat, lng, lbsTime位置信息，传的话，sdk里面不再请求|
|lat|内|纬度|
|lng|内|经度|
|lbsTime|内|获取 lat, lng的系统时间|

<span id = "init-config-provider"></span>

#### 3.2.2 IDefaultConfigProvider（内部项目使用）
如果客户端有应用内默认配置，需要实现该接口。返回一个CustomSlotConfig对象，即广告位的配置。
|方法|说明|
|:-|:-|
|provide(pgtype,slotType)|返回一个ClientSlotConfig对象，即广告位的配置。<br/>pgtype：媒体自定义的广告位名称；<br/>slotType：展示广告的类型，如banner等,参见<a href="#defination_of_slot_type">slotType的定义</a>。|

代码示例：
```java
public class ClientDefaultConfigProvider implements IDefaultConfigProvider {
    @Override
    public ClientSlotConfig provide(String pgtype, String slotType) {
        if (Constants.PGTYPE_FEED.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_FEED, CATEGORY_FEED, 
                    CSJ_APP_ID, CSJ_FEED_ID, 1, 10000);
            slotConfig.add(PLATFORM_HZ, SLOT_TYPE_FEED, CATEGORY_FEED,
                    XM_APP_ID, XM_FEED_ID, 1, 10000);
            return slotConfig;
        }
        return ClientSlotConfig.NONE;
    }
}
```

<span id = "defination_of_slot_type"></span>

#### 3.2.3 slotType的定义（内部项目使用）

|slotType常量|说明|
|:-|:-|
|WSSConstants#SLOT_TYPE_FEED|feed广告|
|WSSConstants#SLOT_TYPE_REWARD_VIDEO|激励视频广告|
|WSSConstants#SLOT_TYPE_SPLASH|开屏广告|
|WSSConstants#SLOT_TYPE_BANNER|Banner广告|
|WSSConstants#SLOT_TYPE_INTERSTITIAL|插屏广告|
|WSSConstants#SLOT_TYPE_DRAW_VIDEO_FEED|竖屏视频广告（用于类抖音视频场景）|

<span id = "init-custom-slot-config"></span>

#### 3.2.4 ClientSlotConfig(内部项目使用)
| 方法                                                         | 说明                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| add(platform,slotType,categoryType,appId,positionId,count,weight) | ClientSlotConfig是广告位的配置，内部持有了一个计划列表，客户端可以通过该方法向列表中添加计划。<br/>platform:广告平台，参见<a href="#defination_of_platform">广告平台的定义</a>；<br/>slotType：展示广告的类型，如banner等,参见<a href="#defination_of_slot_type">slotType的定义</a>;<br/>categoryType：展示广告类型的子类型，如slotType为WSSConstants#SLOT_TYPE_FEED时，子类型可能为categoryType：展示广告类型的子类型，如slotType为WSSConstants#CATEGORY_FEED或WSSConstants#CATEGORY_FEED_TEMPLATE，参见<a href="#defination_of_category_type">categoryType的定义</a>;<br/>appId：媒体在上游广告平台申请的appId；<br/>positionId：广告位id；<br/>count：单次请求几条广告；<br/>weight:这条计划的权重。 |

<span id = "defination_of_platform"></span>

#### 3.2.5 广告平台的定义(内部项目使用)
|platform常量|说明|
|:-|:-|
|WSSConstants#PLATFORM_HZ|禾赞广告|
|WSSConstants#PLATFORM_GDT|广点通广告|
|WSSConstants#PLATFORM_CSJ|穿山甲广告|
|WSSConstants#PLATFORM_KS|快手广告|
|WSSConstants#PLATFORM_BD|百度广告|
|WSSConstants#PLATFORM_JH|聚合广告|

<span id = "defination_of_category_type"></span>

#### 3.2.6 categoryType的定义(内部项目使用)
|categoryType常量|说明|
|:-|:-|
|WSSConstants#CATEGORY_FEED|自渲染feed广告，对应的slotType为WSSConstants#SLOT_TYPE_FEED|
|WSSConstants#CATEGORY_FEED_TEMPLATE|模板feed广告，对应的slotType为WSSConstants#SLOT_TYPE_FEED|
|WSSConstants#CATEGORY_DRAW_VIDEO_FEED|竖版feed视频广告，对应的slotType为WSSConstants#SLOT_TYPE_DRAW_VIDEO_FEED|
|WSSConstants#CATEGORY_REWARD_VIDEO|激励视频，对应的slotType为WSSConstants#SLOT_TYPE_DRAW_VIDEO_FEED|
|WSSConstants#CATEGORY_REWARD_VIDEO2|穿山甲个性化激励视频视频，对应的slotType为WSSConstants#SLOT_TYPE_DRAW_VIDEO_FEED|
|WSSConstants#CATEGORY_INTERSTITIAL|插屏广告，对应的slotType为WSSConstants#SLOT_TYPE_INTERSTITIAL|
|WSSConstants#CATEGORY_INTERSTITIAL_FULLSCREEN_VIDEO|插屏广告，对应的slotType为WSSConstants#SLOT_TYPE_INTERSTITIAL|
|WSSConstants#CATEGORY_SPLASH|开屏广告，对应的slotType为WSSConstants#SLOT_TYPE_SPLASH|
|WSSConstants#CATEGORY_BANNER|Banner广告，对应的slotType为WSSConstants#SLOT_TYPE_BANNER|

<span id = "init-links-provider"></span>

#### 3.2.7 IUrlsProvider（内部项目使用）
WSSAdConfig通过WSSAdConfig#Builder构建，具体方法如下：
|方法|说明|
|:-|:-|
|vtaInfoUrl|请求归因渠道的url|
|locationInfoUrl|（旧）请求地理位置信息的url|
|historyLocationInfoUrl|请求历史位置信息的url|
|hzRequestUrl|请求禾赞广告的url|
|sdkCommonReportUrl|广告通用上报url|
|hbaseLinkUrl|禾赞广告用户画像url|
|sdkRequestReportUrl|广告请求上报url|
|sdkReturnReportUrl|广告返回上报url|
|sdkShowReportUrl|广告展现上报url|
|sdkClickReportUrl|广告点击上报url|
|pollingUrl|云控轮询的url|
|extInfoUrl|云控轮询前置接口的url|
|externalLogUrl|场景广告日志url|
|externalCtrlUrl|请求场景广告云控的url|
|triggerReportUrl|激励视频触发上报url|
|finalPlayUrl|激励视频播放完成上报url|
|newLocationInfoUrl|（新）请求地理位置信息的url|

<span id = "init-xyz-ad-config"></span>

#### 3.2.8 WSSAdConfig的构建
WSSAdConfig通过WSSAdConfig#Builder构建，具体方法如下：
|方法|说明|
|:-|:-|
|setCustomParams(customParams)|设置customParams|
|setDefaultConfigProvider(defaultConfigProvider)|设置defaultConfigProvider|
|setDebug(debug)|是否debug模式，会有关键日志输出|
|setLinksProvider(linksProvider) |sdk用到的接口，如果客户端没有设置会使用sdk内置的|
|setTestServer(isTestServer)|正式服or测试服|

<span id = "load"></span>
### 3.3 WSSAdSdk其他方法介绍
|方法| 作用                       |
|:-|:-|
|WSSAdSdk#onVTAInfoUpdate(String srcPlat, String srcQid)|客户端传入归因和渠道号信息|

## 4. 加载广告

<span id = "load-related-classes"></span>

### 4.1 加载广告用到的类
本章节主要介绍加载广告用到的类。

<span id = "load-mediation-manager"></span>

#### 4.1.1 WSSMediationManager
WSSMediationManager是加载广告的接口，用于加载feed广告、Banner广告、开屏广告、激励视频广告、插屏广告，并提供了回调。
广告sdk初始化之后，开发者可通过以下代码获取IMediationManager对象。

```java
IMediationManager mediationManager = WSSAdSdk.getAdManager();
```
|方法|说明|
|:-|:-|
|loadEmbeddedMaterial(sceneInfo,mediationAdListener)|加载feed广告。<br/>sceneInfo:广告位相关信息，如广告位id；<br/>mediationAdListener:加载广告的回调|
|loadBannerMaterial(sceneInfo,mediationAdListener)|加载Banner广告。<br/>sceneInfo:广告位相关信息，如广告位id；<br/>mediationAdListener:加载广告的回调|
|loadRewardVideoMaterial(sceneInfo,mediationAdListener)|加载激励视频广告。<br/>sceneInfo:广告位相关信息，如广告位id；<br/>mediationAdListener:加载广告的回调|
|loadInterstitialMaterial(sceneInfo,mediationAdListener)|加载插屏广告。<br/>sceneInfo:广告位相关信息，如广告位id；<br/>mediationAdListener:加载广告的回调|
|loadDrawVideoMaterial(sceneInfo,mediationAdListener)|加载竖屏视频广告。<br/>sceneInfo:广告位相关信息，如广告位id；<br/>mediationAdListener:加载广告的回调|
|createSplashManager(pgtype)|为客户端提供SplashManager|
|pollingAdvConfig()|客户端可通过这个方法主动请求云控接口|

<span id = "load-scene-info"></span>

#### 4.1.2 SceneInfo
|方法|说明|
|:-|:-|
|setPgtype|设置广告位的pgtype|
|setSlotType|设置广告位的slotType|
|setSlotWidth|模板feed广告使用，设置广告位的宽度|
|setSlotHeight|模板feed广告使用，设置广告位的高度|
|setOrientation|激励视频广告使用，设置横竖屏，<br/>竖屏：SceneInfo#ORIENTATION_PORTRAIT；<br/>横屏：SceneInfo#ORIENTATION_HORIZONTAL|
|addExtraParameter|客户端定制的一些参数，目前支持：<br/>WSSConstants#EXT_PARAM_GAME_TYPE，合并广告位后可通过gameType区分位置<br/>WSSConstants#EXT_PARAM_VIVO_STYLE，激励视频中途可关闭，取值"0"或"1"<br/>WSSConstants#EXT_PARAM_DIALOG_STYLE，dialog的展示样式，上报服务端<br/>|
|setOverTime|每层策略超时时间，单位ms|
|setUseCacheFirst|优先使用缓存，可减少用户等待时长|
|setAutoCache|设置自动缓存，当缓存中最后一条广告被消耗掉，触发缓存操作|
|setPreload|这次请求是不是预加载，请求禾赞广告时需要这个字段|
|setSplashWait|设置开屏bidding请求逻辑是否要等待最高价的返回|

<span id = "load-mediation-listener"></span>

#### 4.1.3 MediationAdListener
加载广告的回调接口。
|方法|说明|
|:-|:-|
|onLoad(ad)|如果广告请求成功，会回调这个方法。ad即为广告对象。<br/>如果广告被消耗掉返回true，如果广告未被使用返回false|
|onError(error)|如果广告请求失败，会回调这个方法。error为错误信息|

<span id = "load-splash"></span>

### 4.2 加载开屏广告

<span id = "load-splash-code"></span>

#### 4.2.1 代码示例
```java
String pgtype = "splash";
IMediationManager mediationManager = WSSAdSdk.getAdManager();
SceneInfo sceneInfo = new SceneInfo();
sceneInfo.setPgtype(pgtype);
sceneInfo.setSplashWait(false);// false 为方案一，实时广告； true为方案二，死等最高价
ISplashManager splashManager = mediationManager.createSplashManager(pgtype);
splashManager.loadSplash(this, contaier, sceneInfo, new ISplashCallback() {
            @Override
            public void onTimeout() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onTimeout()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdPresent(ISplashMaterial material) {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdPresent()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onError()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdDismiss() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdDismiss()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdClick() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdClick()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdSkip() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdSkip()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCoinRange(String s) {
                Toast.makeText(SplashActivity.this, "SplashActivity::onCoinRange()!", Toast.LENGTH_LONG).show();
            }
        });
```

<span id = "load-splash-manager"></span>

#### 4.2.2 ISplashManager
|方法|说明|
|:-|:-|
|loadSplash(activity, container, sceneInfo, splashCallback)|展示开屏广告，<br/>activity:开屏广告展示的页面；<br/>container：开屏广告容器；<br/>sceneInfo：广告位相关信息；<br/>splashCallback：开屏广告回调接口|

<span id = "load-splash-callback"></span>

#### 4.2.3 ISplashCallback
|方法|说明|
|:-|:-|
|onTimeout()|请求超时|
|onAdPresent(ISplashMaterial material)|开屏广告展示|
|onError()|展示开屏广告出错|
|onAdDismiss()|倒计时结束，回调此方法通知客户端跳转|
|onAdClick()|开屏广告被点击|
|onAdSkip()|跳过按钮被点击|
|onCoinRange(String coinRange)|广告奖励区间|

<span id = "load-feed"></span>

### 4.3 加载feed广告（IEmbeddedMaterial）

<span id = "load-feed-code"></span>

#### 4.3.1 代码示例
```java
IMediationManager mediationManager = WSSAdSdk.getAdManager();
SceneInfo sceneInfo = new SceneInfo();
sceneInfo.setPgtype("feed");
mediationManager.loadFeedAd(sceneInfo,new new MediationAdListener<IEmbeddedMaterial>() {
            @Override
            public boolean onLoad(IEmbeddedMaterial ad) {
                //务必判断页面的状态
                if (!Utils.isActivityAlive(FeedListActivity.this)) {
                    Toast.makeText(FeedListActivity.this, "FeedListActivity not alive!", Toast.LENGTH_LONG).show();
                    return false;//如果广告没有被消耗掉返回false
                }
                if (ad == null) {
                    Toast.makeText(FeedListActivity.this, "no ad", Toast.LENGTH_LONG).show();
                    return false;
                }
                //todo  展现广告
            }
        });
```
<span id = "load-xyz-feed-ad"></span>

#### 4.3.2 IEmbeddedMaterial
|方法|说明|
|:-|:-|
|getTitle()|广告标题|
|getMaterialType()|素材类型<br/>WSSConstants#MATERIAL_UNKNOWN，无法识别的素材类型<br/>WSSConstants#MATERIAL_IMAGE_SMALL，单小图广告<br/>WSSConstants#MATERIAL_IMAGE_GROUP,组图广告<br/>WSSConstants#MATERIAL_IMAGE_LARGE，大图广告<br/>WSSConstants#MATERIAL_VERTICAL_IMAGE，竖图广告<br/>WSSConstants#MATERIAL_VIDEO，视频广告<br/>WSSConstants#MATERIAL_VIDEO_DRAW，竖屏视频<br/>WSSConstants#MATERIAL_TEMPLATE，模板广告<br/>WSSConstants#MATERIAL_VIDEO_REWARD，激励视频广告|
|getDesc()|广告描述|
|getImageList()|广告图片|
|getIconUrl()|广告icon，如果是app广告，则为app的图标|
|isDownload()|是否是下载类广告|
|registerDownloadListener(downloadListener)|下载监听回调|
|unregisterDownloadListener(downloadListener)|移除下载监听|
|getSource()|广告源|
|onPause()|离开广告展示页面时需要回调此方法，否则视频广告状态可能会出错|
|onResume()|返回广告展示页面时需要回调此方法，否则视频广告状态可能会出错|
|pauseVideo()|广告展示区域不可见时需要回调此方法，否则视频广告状态可能会出错|
|resumeVideo()|广告展示区域可见时需要回调此方法，否则视频广告状态可能会出错|
|getPlatform()|广告平台，参见<a href="#defination_of_platform">广告平台的定义</a>|
|appendExtraParameters(String key, String value)||
|getAppName()|如果是app类广告，尝试返回app的名字，失败返回null|
|bindView(view,clickViewList,creativeViewList,closeView,interactionListener)|绑定点击事件<br/>view:开发者根据素材所构建的view；<br/>clickViewList:可点击的view列表；<br/>creativeViewList：可点击创意view列表；<br/>closeView： 如果所构建的广告view包含‘关闭’操作，则此参数值为关闭对应的view；<br/>interactionListener：监听广告的曝光和点击,<font color="#f33">开发者一定要将bindView接口返回的View添加到容器中进行展示，否则可能会出现无法点击等异常影响收益</font>|
|attach(Activity activity)|如果广告在activity中展示，需要调用此方法（兼容聚合广告）|
|attach(Dialog dialog)|如果广告在dialog中展示，需要调用此方法（兼容聚合广告）|
|bindMediaView(mediaView, videoOptions, mediaListener)|如果getMaterialType()方法返回了WSSConstants#MATERIAL_VIDEO,需要调用此方法处理视频区域的逻辑。<br/>mediaView：视频展示容器，必须是MediaView实例；<br/>videoOptions:视频播放的参数<br/>mediaListener:播放视频的回调接口，目前只有广点通广告支持|
|getDownloadStatus()|获取下载状态DownloadStatus|


<span id = "load-reward-video"></span>

### 4.4 加载激励视频广告（IRewardVideoMaterial）

<span id = "load-reward-video-code"></span>

#### 4.4.1 代码示例
```java
IMediationManager mediationManager = WSSAdSdk.getAdManager();
SceneInfo sceneInfo = new SceneInfo();
sceneInfo.setPgtype("reward_video");
mediationManager.loadRewardVideoAd(sceneInfo,new new MediationAdListener<IRewardVideoMaterial>() {
            @Override
            public boolean onLoad(IRewardVideoMaterial ad) {
                //务必判断页面的状态
                if (!Utils.isActivityAlive(RewardVideoActivity.this)) {
                    Toast.makeText(RewardVideoActivity.this, "RewardVideoActivity not alive!", Toast.LENGTH_LONG).show();
                    return false;//如果广告没有被消耗掉返回false
                }
                if (ad == null) {
                    Toast.makeText(RewardVideoActivity.this, "no ad", Toast.LENGTH_LONG).show();
                    return false;
                }
                //todo  展现广告
            }
        });
```

<span id = "load-xyz-reward-video-ad"></span>

#### 4.4.2 IRewardVideoMaterial
|方法|说明|
|:-|:-|
|show(activity,rewardVideoListener)|展示激励视频广告<br/>activity:调用页面activity；<br/>rewardVideoListener：监听播放结果|

<span id = "load-reward-video-listener"></span>

#### 4.4.3 IRewardVideoListener
|方法|说明|
|:-|:-|
|onComplete(rewardVideoResult)|播放激励视频的结果，开发者可根据RewardVideoResult#isVerified()决定要不要发分|
|onError(rewardVideoError)|播放激励视频发生错误，rewardVideoError为错误类型|

<span id = "load-interstitial"></span>

### 4.5 加载插屏广告(IInterstitialMaterial)

<span id = "load-interstitial-code"></span>

#### 4.5.1 代码示例
```java
IMediationManager mediationManager = WSSAdSdk.getAdManager();
SceneInfo sceneInfo = new SceneInfo();
sceneInfo.setPgtype("interstitial");
mediationManager.loadInterstitialAd(sceneInfo,new new MediationAdListener<IInterstitialMaterial>() {
            @Override
            public boolean onLoad(IInterstitialMaterial ad) {
                //务必判断页面的状态
                if (!Utils.isActivityAlive(InterstitialActivity.this)) {
                    Toast.makeText(InterstitialActivity.this, "InterstitialActivity not alive!", Toast.LENGTH_LONG).show();
                    return false;//如果广告没有被消耗掉返回false
                }
                if (ad == null) {
                    Toast.makeText(InterstitialActivity.this, "no ad", Toast.LENGTH_LONG).show();
                    return false;
                }
                //todo  展现广告
            }
        });
```

<span id = "load-xyz-interstitial-ad"></span>

#### 4.5.2 IInterstitialMaterial
|方法|说明|
|:-|:-|
|show(activity,interstitialListener)|展示插屏广告<br/>activity:调用页面activity；<br/>rewardVideoListener：监听播放结果|

<span id = "load-interstitial-listener"></span>

#### 4.5.3 IInterstitialListener
|方法|说明|
|:-|:-|
|onAdShow()|插屏广告展现|
|onAdClick()|插屏广告被点击|
|onAdClose()|通知客户端广告被关闭|

<span id = "load-banner"></span>

### 4.6 加载Banner广告（IBannerMaterial）

<span id = "load-banner-code"></span>

#### 4.6.1 代码示例
```java
IMediationManager mediationManager = WSSAdSdk.getAdManager();
SceneInfo sceneInfo = new SceneInfo();
sceneInfo.setPgtype("banner");
<font color="#ff3">设置Banner广告的宽和高</font>
mediationManager.loadBannerAd(sceneInfo,new new MediationAdListener<IBannerMaterial>() {
            @Override
            public boolean onLoad(IBannerMaterial ad) {
                //务必判断页面的状态
                if (!Utils.isActivityAlive(BannerActivity.this)) {
                    Toast.makeText(BannerActivity.this, "BannerActivity not alive!", Toast.LENGTH_LONG).show();
                    return false;//如果广告没有被消耗掉返回false
                }
                if (ad == null) {
                    Toast.makeText(BannerActivity.this, "no ad", Toast.LENGTH_LONG).show();
                    return false;
                }
                //todo  展现广告
            }
        });
```

<span id = "load-xyz-banner-ad"></span>

#### 4.6.2 IBannerMaterial
|方法|说明|
|:-|:-|
|show(activity,gravity,x,y,showBorder,materialInteractionListener)|展示Banner广告<br/>activity:调用页面activity；<br/>gravity：banner展示的gravity；<br/>x:在gravity基础上的距离左侧或右侧的距离，如果居中传0；<br/>x:在gravity基础上的距离顶部或底部的距离，如果居中传0；<br/>showBorder：是否展示边框<br/>materialInteractionListener：监听广告的展现、点击和关闭<br/>|
|show(container,materialInteractionListener)|展示Banner广告<br/>container:banner广告展示的容器<br/>materialInteractionListener：监听广告的展现、点击和关闭|
|dismiss()|移除banner广告|

<span id = "load-material-interaction-listener"></span>

#### 4.6.3 IMaterialInteractionListener
|方法|说明|
|:-|:-|
|onAdShow()|广告展现|
|onAdClick()|广告点击|
|onAdvClose()|广告关闭|