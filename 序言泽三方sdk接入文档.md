# 序言泽保活SDK接入文档
[TOC]

# 1 背景

​		项目在接入中台sdk，需要检查接入的三方sdk是否匹配


#  2 三方sdk
|三方广告sdk|版本|
|:-|:-|
|穿山甲|3809|
|广点通| 4.390.1260          |
|百度| 9.15               |
|聚合| 111004_202108241533 |
|快手| 3.3.20.2            |
|游可盈| 2.2.0.21            |

# 3 步骤
## 3.1 导入aar

请根据项目需要，将相应三方SDK复制到libs文件夹(没有的话须手动创建), 并将以下代码添加到您app的build.gradle中：

```shell script
implementation fileTree(dir: 'libs', include: ['*.jar']) 
implementation(name: 'csj_xx', ext: 'aar') //穿山甲
implementation(name: 'GDTSDK.xx', ext: 'aar') //广点通
implementation(name: 'JHSDK_xx', ext: 'aar') //聚合 SDK1 
implementation(name: 'DB_xx', ext: 'aar') //聚合 SDK2 
implementation(name: 'Baidu_xx', ext: 'aar') //百度
implementation(name: 'kssdk-xx', ext: 'aar') //快手
implementation(name: 'klevinSDK_xx', ext: 'aar') //游可赢
```

## 3.2 组件配置

### 3.2.1 穿山甲
```java
		<provider
            android:name="com.bykv.vk.openvk.TTFileProvider"
            android:authorities="${applicationId}.TTFileProvider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/csj_file_path" />
        </provider>

        <provider
            android:name="com.bykv.vk.openvk.multipro.TTMultiProvider"
            android:authorities="${applicationId}.TTMultiProvider"
            android:exported="false" />
```

### 3.2.2 gdt 
```shell script
		<provider
            android:name="com.qq.e.comm.GDTFileProvider"
            android:authorities="${applicationId}.gdt.fileprovider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/gdt_file_path" />
        </provider>
```
### 3.2.3 百度 
   无
   
### 3.2.4 快手
   无

### 3.2.5 聚合

   无
   
### 3.2.6 游可赢 
```shell script
        <provider
            android:name="com.tencent.klevin.utils.FileProvider"
            android:authorities="${applicationId}.klevin.fileProvider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/klevin_provider_paths"/>
        </provider>
```

## 3.3 添加混淆

### 3.3.1 穿山甲

```java
-keep class com.bytedance.sdk.openadsdk.** { *; }
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.** {*;}
-keep class ms.bd.c.**{*;}
-keep class com.bytedance.mobsec.**{*;}
-keep class com.bytedance.embed_dr.** {*;}
-keep class com.bytedance.embedapplog.** {*;}
-keep interface com.bytedance.frameworks.baselib.network.http.cronet.I* {*;}
-keepnames class com.bytedance.framwork.core.sdkmonitor.SDKMonitorUtils
```
如果您的应⽤启⽤了资源混淆或资源缩减，您需要保留SDK的资源。
详见：穿山甲sdk中whiteList.txt文档


### 3.3.2 gdt 

```shell script
-keep class com.qq.e.** {
    public protected *;
}
-keep class android.support.v4.**{
    public *;
}
-keep class android.support.v7.**{
    public *;
}
```

### 3.3.3 百度 

```shell script
-ignorewarnings
-dontwarn com.qsmy.walkmonkey.api.**
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.qsmy.walkmonkey.api.** { *; }
-keep class com.style.widget.** {*;}
-keep class com.component.** {*;}
-keep class com.baidu.ad.magic.flute.** {*;}
-keep class com.baidu.mobstat.forbes.** {*;}
```

### 3.3.4 快手

```shell script
-keep class org.chromium.** {*;}
-keep class org.chromium.** { *; }
-keep class aegon.chrome.** { *; }
-keep class com.kwai.**{ *; }
-dontwarn com.kwai.**
-dontwarn com.kwad.**
-dontwarn com.ksad.**
-dontwarn aegon.chrome.**
```

如果您的应⽤启⽤了资源混淆或资源缩减，您需要保留SDK的资源，SDK的资源名都是以ksad_开头

的。您可以在资源混淆配置⽂件添加如下配置：

```shell script
"R.string.ksad*",
"R.layout.ksad*",
"R.drawable.ksad*",
"R.anim.ksad*",
"R.color.ksad*",
"R.style.ksad*",
"R.id.ksad*",
"R.xml.ksad*",
"R.dimen.ksad*",
"R.attr.ksad*",
```

### 3.3.5 聚合

```shell script
-keep class com.voguetool.sdk.*.** {*;}
-keep class com.sunrisehelper.sdk.*.** {*;}

-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
```
如果您的应⽤启⽤了资源混淆或资源缩减，您需要保留SDK的资源，SDK的资源名都是以jhsdk_开头

的。您可以在资源混淆配置⽂件添加如下配置：

```shell script
"R.string.jhsdk_*",
"R.layout.jhsdk_*",
"R.drawable.jhsdk_*",
"R.anim.jhsdk_*",
"R.color.jhsdk_*",
"R.style.jhsdk_*",
"R.id.jhsdk_*",
"R.xml.jhsdk_*",
"R.attr.jhsdk_*",
"R.dimen.jhsdk_*",
"R.raw.jhsdk_*",
```