# 序言泽保活SDK接入文档

# 1 更新记录

|保活版本|修改日期|修改日志|修改人|
|:-|:-|:-|:-|
|自研保活：1.1.0  \|  安天保活：1.1.0|2021.07.30|初版|缪志勇|


#  2 背景
目前保活有自研保活和安天保活，客户端按需接入其中一种即可

# 3 保活功能
## 3.1 自研保活功能

 Tip：		 

​		自研保活功能依附于主体广告xmsdk，不能独立使用；

### 3.1.1 导入aar

将keepalive_XXX.aar和weather.aar复制到libs文件夹(没有的话须手动创建), 并将以下代码添加到您app的build.gradle中：

```shell script
depedencies {
    api(name: 'adv_keep_alive-release', ext: 'aar')
    api(name: 'adv_weather-release', ext: 'aar')
}
```

### 3.1.2 保活添加混淆

```shell script
-keep class com.wss.bbb.e.keeplive.daemon.component.WssInstrumentation{*;}
-keep class com.wss.bbb.e.keeplive.daemon.NativeKeepAlive{*;}
-keep class com.wss.bbb.e.keeplive.daemon.DaemonMain{*;}
```


### 3.1.3 AndroidManifest和build.gradle的配置

####      在manifest里面添加instrumentation要和application标签同级
```xml
<instrumentation
        android:name="com.wss.bbb.e.keeplive.daemon.component.WssInstrumentation"
        android:targetPackage="${applicationId}"
        android:targetProcesses="${applicationId},${applicationId}:wssresident,${applicationId}:wssdaemon,${applicationId}:wssassist1,${applicationId}:wssassist2" />
```
### 3.1.4 保活功能初始化

#### 3.1.4.1 在application#onCreate()方法中 **(不要限制主进程)** 调用 WSSKeepLive.init(Application,ICustomNotificationCreator)
```java
 /**
     * 在application#onCreate()方法中 **(不要限制主进程)** 调用
     *
     * @param application
     * @param customNotificationCreator
     */
    public static void init(Application application, ICustomNotificationCreator customNotificationCreator) {
    }
```
#### 3.1.4.2 在MainActivity#onCreate()方法中调用 WSSKeepLive.init2(Context,Intent)
```java
 /**
     * 在MainActivity#onCreate()方法中调用。
     * 如果客户端自定义常驻通知栏，可以把数据打包到intent中，
     * 广告sdk调用ICustomNotificationCreator#createNotification(Context context, Intent intent)时（在packageName:resident进程），
     * 会把intent作为参数原封返回给客户端
     *
     * @param context
     * @param intent
     */
    public static void init2(Context context, Intent intent) {
    }
```

### 3.1.5 so兼容性

广告sdk仅提供了armeabi-v7a的so，如果客户端app需要支持其它架构，请与广告sdk开发联系

### 3.1.6客户端自定义常驻通知栏

可以不用自定义，直接使用默认的天气通知栏即可

#### 3.1.6.1 初始化时传入自定义的ICustomNotificationCreator，并按需实现它的方法

```java
public interface ICustomNotificationCreator {
    /**
     * @return 常驻通知smallIcon, 使用sdk默认请传0
     */
    int getSmallIconResId();

    /**
     * @return 常驻通知LargeIcon, 没有该项请传0
     */
    int getLargeIconResId();

    /**
     * @return 常驻通知栏模式
     * 1. 客户端自定义
     * 2. 简单天气项目使用push数据
     * other. 内置样式
     */
    int notificationMode();

    /**
     * 该方法在packageName:resident进程中回调，请注意跨进程数据处理
     *
     * @param context
     * @param intent  重要！！！
     *                1. 可能为null，
     *                2. 可能是客户端调用KeepLive.updateCustomNotification(Context context, Intent intent)
     *                或KeepLive.init2(Context,intent)传入的intent，这里作为参数原封不动给到客户端。
     *                3. 也可能不是客户端传进来的intent。
     *                4. 建议客户端判断intent中是否携带数据，如果携带的话，在进程中做持久化存储，
     *                后续发现intent中不携带数据时，可直接从持久化存储中取出数据填充notification
     *                5. 该方法在启动时可能会被调用多次
     *                6. 客户端可能需要在该方法中规划下次刷新
     * @return 客户端实现的Notification的创建，仅需创建Notification，不要处理NotificatinChannel，不能返回null
     */
    Notification createNotification(Context context, Intent intent);


    /**
     * 用户点击通知栏的接收的receiver类
     */
    Class getReceiverClass();

}
```
#### 3.1.6.2 客户端需要更新notification的时候请调用

```java

WSSKeepLive.updateCustomNotification(Context context, Intent intent);
```


## 3.2 安天保活功能		 

​	说明：

​		 对火凤凰 (firephoenix-sdk-1.1.0.aar) 进行封装后输出对应aar，方便客户端调用

### 3.2.1 android版本支持
⽀持Android5.0 - Android11 ，即项目minsdk版本必须是17

### 3.2.2 导入aar

将keepalive_antian.aar复制到libs文件夹(没有的话须手动创建), 并将以下代码添加到您app的build.gradle中：

```shell script
depedencies {
    api(name: 'keepalive_antian', ext: ‘aar')
}
```

### 3.2.3 保活添加混淆

```shell script
-keep class com.fire.phoenix.**{*;}
```

### 3.2.4 保活功能初始化

#### 3.2.4.1 在application#onCreate()方法中 **(不要限制主进程)** 调用 WSSKeepLive.init(Application,Notification)
```java
 /**
     * 在application#onCreate()方法中 **(不要限制主进程)** 调用
     *
     * @param application
     * @param notification  //设置前台通知，也可以传null使用默认的
     */
    public static void init(Application application, Notification notification) {
    }
```
#### 3.2.4.2 在MainActivity#onCreate()方法中调用 WSSKeepLive.init2()