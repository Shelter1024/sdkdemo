﻿# 序言泽SDK更新日志
# 2021.08.31
**修改内容：**
1. 修复百度内容联盟退出后拉起应用的问题

# 2021.09.01
**修改内容：**
1. 修改穿山甲混淆规则，详见序言泽三方sdk接入文档
```java
-keep class com.bytedance.sdk.openadsdk.** { *; }
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.** {*;}
-keep class ms.bd.c.**{*;}
-keep class com.bytedance.mobsec.**{*;}
-keep class com.bytedance.embed_dr.** {*;}
-keep class com.bytedance.embedapplog.** {*;}
-keep interface com.bytedance.frameworks.baselib.network.http.cronet.I* {*;}
-keepnames class com.bytedance.framwork.core.sdkmonitor.SDKMonitorUtils
```
2. adv_core-priv-release的manifest文件中添加中台SDK版本号和打包时间
3. 修复DSP激励视频倒计时为0的问题

# 2021.09.03
**修改内容：**
1. 补充中台ijkplayer-release.aar的混淆规则 
-keep class com.ijk.ijkplayer.IjkVideoView{*;}
