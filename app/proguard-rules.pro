# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keepattributes Signature
-keepattributes *Annotation*

# 聚合 start #
-keep class com.voguetool.sdk.*.** {*;}
-keep class com.sunrisehelper.sdk.*.** {*;}

-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
# 聚合 end #

# 穿山甲 start #
-keep class com.bytedance.sdk.openadsdk.** { *; }
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.* {*;}
# 穿山甲 end  #

# baidu  -- start
-ignorewarnings
-dontwarn com.qsmy.walkmonkey.api.**
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.qsmy.walkmonkey.api.** { *; }
-keep class com.style.widget.** {*;}
-keep class com.component.** {*;}
-keep class com.baidu.ad.magic.flute.** {*;}
-keep class com.baidu.mobstat.forbes.** {*;}
# baidu  -- end

#---kuaishou start---
-keep class org.chromium.** {*;}
-keep class org.chromium.** { *; }
-keep class aegon.chrome.** { *; }
-keep class com.kwai.**{ *; }
-keepclasseswithmembernames class * {
    native <methods>;
}
-dontwarn com.kwai.**
-dontwarn com.kwad.**
-dontwarn com.ksad.**
-dontwarn aegon.chrome.**
#---kuaishou end---

#---ijkplayer start---
-keep class tv.danmaku.ijk.media.player.** {*; }
-keepclasseswithmembernames class tv.danmaku.ijk.media.player.IjkMediaPlayer{
public <fields>;
public <methods>;
}
-keepclasseswithmembernames class tv.danmaku.ijk.media.player.ffmpeg.FFmpegApi{
public <fields>;
public <methods>;
}
#---ijkplayer end---

#---中台sdk （core module未混淆过的）start---
#点击优化 start
-keep class com.wss.bbb.e.mediation.optimize.IAdUtils{*;}
-keep class com.wss.bbb.e.mediation.source.Material{
    public void updateCeffect(***);
    public *** getAdv(***);
}
-keep class com.wss.bbb.e.mediation.optimize.OptimizeStrategy{*;}
#点击优化 end

#EventBus start
-keepattributes *Annotation*
-keep class com.wss.bbb.e.eventbus.Subscribe{*;}
-keepclassmembers class * {
    @com.wss.bbb.e.eventbus.Subscribe <methods>;
}
-keep class com.wss.bbb.e.eventbus.ThreadMode { *; }
#EventBus end
-keep class com.wss.bbb.e.utils.SystemMarker{*;}
-keep class com.coke.**{*;}
#---中台sdk end---

#安天保活 start
-keep class com.fire.phoenix.**{*;}
#安天保活 end

#自研保活 start
-keep class com.wss.bbb.e.keeplive.daemon.component.WssInstrumentation{*;}
-keep class com.wss.bbb.e.keeplive.daemon.NativeKeepAlive{*;}
-keep class com.wss.bbb.e.keeplive.daemon.DaemonMain{*;}
#自研保活 end