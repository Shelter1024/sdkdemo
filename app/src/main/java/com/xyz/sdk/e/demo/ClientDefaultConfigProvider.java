package com.xyz.sdk.e.demo;

import com.wss.bbb.e.mediation.api.IDefaultConfigProvider;
import com.wss.bbb.e.mediation.config.ClientSlotConfig;

import static com.wss.bbb.e.WSSConstants.CATEGORY_BANNER;
import static com.wss.bbb.e.WSSConstants.CATEGORY_DRAW_VIDEO_FEED;
import static com.wss.bbb.e.WSSConstants.CATEGORY_FEED;
import static com.wss.bbb.e.WSSConstants.CATEGORY_INTERSTITIAL;
import static com.wss.bbb.e.WSSConstants.CATEGORY_REWARD_VIDEO;
import static com.wss.bbb.e.WSSConstants.CATEGORY_SPLASH;
import static com.wss.bbb.e.WSSConstants.PLATFORM_CSJ;
import static com.wss.bbb.e.WSSConstants.PLATFORM_GDT;
import static com.wss.bbb.e.WSSConstants.PLATFORM_HZ;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_BANNER;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_DRAW_VIDEO_FEED;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_FEED;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_INTERSTITIAL;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_REWARD_VIDEO;
import static com.wss.bbb.e.WSSConstants.SLOT_TYPE_SPLASH;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_APP_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_BANNER_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_DRAW_VIDEO_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_FEED_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_INTERSTITIAL_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_REWARD_VIDEO_ID;
import static com.xyz.sdk.e.demo.ClientConstants.CSJ_SPLASH_ID;
import static com.xyz.sdk.e.demo.ClientConstants.GDT_APP_ID;
import static com.xyz.sdk.e.demo.ClientConstants.GDT_FEED_ID;
import static com.xyz.sdk.e.demo.ClientConstants.GDT_INTERSTITIAL_ID;
import static com.xyz.sdk.e.demo.ClientConstants.XM_APP_ID;
import static com.xyz.sdk.e.demo.ClientConstants.XM_FEED_ID;
import static com.xyz.sdk.e.demo.ClientConstants.XM_INTERSTITIAL_ID;

public class ClientDefaultConfigProvider implements IDefaultConfigProvider {
    @Override
    public ClientSlotConfig provide(String pgtype, String slotType) {
        if (Constants.PGTYPE_FEED.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
//            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_FEED, CATEGORY_FEED, CSJ_APP_ID, CSJ_FEED_ID, 1, 10000);
            slotConfig.add(PLATFORM_GDT, SLOT_TYPE_FEED, CATEGORY_FEED, GDT_APP_ID, GDT_FEED_ID, 1, 10000);
            return slotConfig;
        }

        if (Constants.PGTYPE_REWARD_VIDEO.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_REWARD_VIDEO, CATEGORY_REWARD_VIDEO, CSJ_APP_ID, CSJ_REWARD_VIDEO_ID, 1, 10000);
            return slotConfig;
        }

        if (Constants.PGTYPE_DRAW_VIDEO.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_DRAW_VIDEO_FEED, CATEGORY_DRAW_VIDEO_FEED, CSJ_APP_ID, CSJ_DRAW_VIDEO_ID, 1, 10000);
            return slotConfig;
        }

        if (Constants.PGTYPE_SPLASH.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_SPLASH, CATEGORY_SPLASH, CSJ_APP_ID, CSJ_SPLASH_ID, 1, 10000);
            return slotConfig;
        }

        if (Constants.PGTYPE_INTERSTITIAL.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_INTERSTITIAL, CATEGORY_INTERSTITIAL, CSJ_APP_ID, CSJ_INTERSTITIAL_ID, 1, 10000);
            slotConfig.add(PLATFORM_GDT, SLOT_TYPE_INTERSTITIAL, CATEGORY_INTERSTITIAL, GDT_APP_ID, GDT_INTERSTITIAL_ID, 1, 10000);
            slotConfig.add(PLATFORM_HZ, SLOT_TYPE_INTERSTITIAL, CATEGORY_INTERSTITIAL,  XM_APP_ID, XM_INTERSTITIAL_ID, 1, 10000);
            return slotConfig;
        }

        if (Constants.PGTYPE_BANNER.equals(pgtype)) {
            ClientSlotConfig slotConfig = new ClientSlotConfig();
            slotConfig.add(PLATFORM_CSJ, SLOT_TYPE_BANNER, CATEGORY_BANNER, CSJ_APP_ID, CSJ_BANNER_ID, 1, 10000);
            return slotConfig;
        }
        return ClientSlotConfig.NONE;
    }
}

