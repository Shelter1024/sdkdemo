package com.xyz.sdk.e.demo;

public class ClientConstants {
    //demo  正式服
//    public static final String APP_TYPE_ID = "300001";
    public static final String XM_APP_ID = "41matx01";
    public static final String XM_FEED_ID = "42599900710201";
    public static final String XM_REWARD_VIDEO_ID = "22086792210201";
    public static final String XM_SPLASH_ID = "42611853570301";
    public static final String XM_INTERSTITIAL_ID = "42599900710201";


    //步多多
    public static final String PKG_NAME = "com.qsmy.walkmonkey";
    public static final String APP_TYPE_ID = "100001";
    public static final String APP_NAME = "步多多";
    public static final String SOFT_TYPE = "100001";
    public static final String SOFT_NAME = "100001";

    public static final String GDT_APP_ID = "1109688949";
    public static final String CSJ_APP_ID = "5025018";
    public static final String KS_APP_ID = "502800009";
    public static final String JH_APP_ID = "681";
    public static final String MTG_APP_ID = "138648";
    public static final String MTG_APP_KEY = "187a519185e62bd018f9ccff294a03ec";
    //好宝宝
    public static final String BD_APP_ID = "be80f97c";

    public static final String CSJ_FEED_ID = "925018919";
    public static final String CSJ_REWARD_VIDEO_ID = "945042499";
    public static final String CSJ_SPLASH_ID = "825018922";
    public static final String CSJ_DRAW_VIDEO_ID = "945434819";
    public static final String CSJ_INTERSTITIAL_ID = "";
    public static final String CSJ_BANNER_ID = "";



    public static final String GDT_FEED_ID = "9001742461817660";
    public static final String GDT_REWARD_VIDEO_ID = "5021148480351364";
    public static final String GDT_SPLASH_ID = "5040288190982434";
    public static final String GDT_INTERSTITIAL_ID = "1041543421913841";
    public static final String GDT_DRAW_VIDEO_ID = "5031540440853439";

    public static final String KS_FEED_ID = "5028000056";
    public static final String KS_REWARD_VIDEO_ID = "5028000055";
    public static final String KS_SPLASH_ID = "5028000129";
    public static final String KS_DRAW_VIDEO_ID = "5028000085";

    public static final String JH_FEED_ID = "NW21603297";
    public static final String JH_REWARD_VIDEO_ID = "NW21603296";
    public static final String JH_SPLASH_ID = "NW21603298";
    public static final String JH_INTERSTITIAL_ID = "NW21603299";
    public static final String JH_DRAW_VIDEO_ID = "NW21603305";

    public static final String BD_FEED_ID = "7310304";
    public static final String BD_REWARD_VIDEO_ID = "7297956";
    public static final String BD_SPLASH_ID = "7297958";
    public static final String BD_DRAW_VIDEO_ID = "";
    public static final String BD_INTERSTITIAL_ID = "7297925";

    public static final String PGTYPE_FEED = "feed";
    public static final String PGTYPE_REWARD_VIDEO = "reward_video";

    public static final String PGTYPE_DRAW_VIDEO = "draw_video";
    public static final String PGTYPE_INTERSTITIAL = "interstitial";
    public static final String PGTYPE_OPEN = "open";
    public static final boolean TEST_SERVER = false;
    public static final boolean DEBUG = false;
}
