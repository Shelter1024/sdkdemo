package com.xyz.sdk.e.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.wss.bbb.e.mediation.ISplashManager;
import com.wss.bbb.e.mediation.WSSMediationManager;
import com.wss.bbb.e.mediation.api.ISplashCallback;
import com.wss.bbb.e.mediation.source.ISplashMaterial;
import com.wss.bbb.e.mediation.source.SceneInfo;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        FrameLayout contaier = findViewById(R.id.container);
        ISplashManager splashManager = WSSMediationManager.getInstance().createSplashManager(Constants.PGTYPE_SPLASH);
        SceneInfo sceneInfo = new SceneInfo();
        sceneInfo.setPgtype(Constants.PGTYPE_SPLASH);
        //        sceneInfo.setSplashWait(false);
        splashManager.loadSplash(this, contaier, sceneInfo, new ISplashCallback() {
            @Override
            public void onTimeout() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onTimeout()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdPresent(ISplashMaterial material) {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdPresent()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onError()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdDismiss() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdDismiss()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdClick() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdClick()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdSkip() {
                Toast.makeText(SplashActivity.this, "SplashActivity::onAdSkip()!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCoinRange(String s) {
                Toast.makeText(SplashActivity.this, "SplashActivity::onCoinRange()!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
