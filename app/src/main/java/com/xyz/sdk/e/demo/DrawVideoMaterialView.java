package com.xyz.sdk.e.demo;

import android.content.Context;

import com.wss.bbb.e.display.BaseMaterialView;

public class DrawVideoMaterialView extends BaseMaterialView {
    public DrawVideoMaterialView(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.adv_material_view_draw_video;
    }
}
