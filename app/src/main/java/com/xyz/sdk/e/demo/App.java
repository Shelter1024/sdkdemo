package com.xyz.sdk.e.demo;

import android.app.Application;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.wss.bbb.e.WSSAdSdk;
import com.wss.bbb.e.WSSConstants;
import com.wss.bbb.e.common.IActivityLifecycleObservable;
import com.wss.bbb.e.components.CM;
import com.wss.bbb.e.keeplive.WSSKeepLive;
import com.wss.bbb.e.keeplive.notification.ICustomNotificationCreator;
import com.wss.bbb.e.scene.ActivityClickListener;
import com.wss.bbb.e.scene.ActivityData;
import com.wss.bbb.e.scene.ISceneController;
import com.wss.bbb.e.scene.SceneConfig;
import com.wss.bbb.e.scene.WSSSceneSdk;
import com.wss.bbb.e.scene.WallpaperListener;
import com.xyz.sdk.e.demo.utils.AppProcessUtil;

import java.util.HashMap;
import java.util.Map;

public class App extends Application {
    private static App app;

    public static Context getContext() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.app = this;
        if (AppProcessUtil.isMainProcess(this)) {
            WSSAdSdk.preInit(this);
            AdSdkInitializer.init(this);
            initSceneSdk(this);
        }
        WSSKeepLive.init(app, new ICustomNotificationCreator() {
            @Override
            public int getSmallIconResId() {
                return 0;
            }

            @Override
            public int getLargeIconResId() {
                return 0;
            }

            @Override
            public int notificationMode() {
                return 0;
            }

            @Override
            public Notification createNotification(Context context, Intent intent) {
                return null;
            }

            @Override
            public Class getReceiverClass() {
                return null;
            }
        });
        //WSSKeepLive.init(app,null);  //安天保活
    }

    private void initSceneSdk(Application app) {
        SceneConfig sceneConfig = new SceneConfig.Builder()
                .sceneController(new ISceneController() {
                    @Override
                    public boolean isSceneOn(int scene) {
                        return true;
                    }

                    @Override
                    public String wpBgName() {
                        return "ic_launcher";
                    }


                    @Override
                    public Map<Integer, Map<Integer, String>> getPageTypeConfigMap() {
                        Map<Integer, Map<Integer, String>> map = new HashMap<>();
                        Map<Integer, String> backstageMap = new HashMap<>();
                        backstageMap.put(WSSConstants.AD_TYPE_REWARD_VIDEO, "rewardvideonormal");
                        backstageMap.put(WSSConstants.AD_TYPE_FEED, "bignormal");
                        backstageMap.put(WSSConstants.AD_TYPE_FULLSCREEN_VIDEO, "videonormal");
                        backstageMap.put(WSSConstants.AD_TYPE_INTERSTITIAL, "insertnormal");
                        backstageMap.put(WSSConstants.AD_TYPE_SPLASH, "open");
                        map.put(WSSConstants.SCENE_BACKSTAGE, backstageMap);
                        return map;
                    }
                })
                .activityClickListener(new ActivityClickListener() {
                    @Override
                    public void onActivityClick(ActivityData activityData) {

                    }
                })
                .wallpaperListener(new WallpaperListener() {
                    @Override
                    public boolean shouldSetWallpaper() {
                        return true;
                    }

                    @Override
                    public void onWallpaperSettingsPageShow() {

                    }

                    @Override
                    public void onWallpaperSettingsPageClose(int type, int state, String wpCode) {

                    }

                    @Override
                    public void onWallpaperSettingsPageFailShow(String wpCode) {

                    }

                    @Override
                    public Drawable takeWpDrawable() {
                        return null;
                    }

                    @Override
                    public String takeWpCode() {
                        return null;
                    }

                    @Override
                    public void onConditionResult(boolean result) {

                    }
                })
                .build();
        WSSSceneSdk.init(this, sceneConfig);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * 初始化移动安全联盟
     */
    private static void initMobileSafeSdk() {
//        MiitHelper.getAndroidDeviceIds(new MiitHelper.OaidListener() {
//            @Override
//            public void onGetSuccess() {
//
//            }
//        });
    }


    @Override
    public void startActivity(Intent intent) {
//        Log.d("travis", "App::startActivity()  clazzName=" + intent.getComponent().getShortClassName());
        boolean handled = WSSSceneSdk.onStartActivity(this, intent);
//        Log.d("travis", "App::startActivity()#2  clazzName=" + intent.getComponent().getShortClassName());
        if (handled) {
            return;
        } else {
//            Log.d("travis", "App::startActivity()#3  clazzName=" + intent.getComponent().getShortClassName());
            super.startActivity(intent);
        }
    }


    @Override
    public Object getSystemService(String name) {
//        if (Context.WIFI_SERVICE.equals(name) && !CM.use(IActivityLifecycleObservable.class).isAppForeground()) {
//            Log.d("travis", "App::getSystemService()");
//            new Exception().printStackTrace();
//        }
        return super.getSystemService(name);
    }
}
