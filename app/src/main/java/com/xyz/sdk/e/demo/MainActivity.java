package com.xyz.sdk.e.demo;


import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wss.bbb.e.WSSAdSdk;
import com.wss.bbb.e.common.IMultiProcessSPUtils;
import com.wss.bbb.e.common.ISPUtils;
import com.wss.bbb.e.components.CM;
import com.wss.bbb.e.keeplive.daemon.AppProcessThread;
import com.wss.bbb.e.keeplive.daemon.component.WssAssistService1;
import com.wss.bbb.e.keeplive.daemon.component.WssAssistService2;
import com.wss.bbb.e.keeplive.daemon.component.WssDaemonService;
import com.wss.bbb.e.keeplive.daemon.component.WssInstrumentation;
import com.wss.bbb.e.keeplive.main.WssNotifyResidentService;
import com.wss.bbb.e.utils.SystemMarker;
import com.xyz.sdk.e.demo.utils.Utils;
import com.wss.bbb.e.keeplive.WSSKeepLive;
import com.wss.bbb.e.mediation.api.IInterstitialListener;
import com.wss.bbb.e.mediation.api.IMaterialInteractionListener;
import com.wss.bbb.e.mediation.api.IRewardVideoListener;
import com.wss.bbb.e.mediation.api.AdvMediationListener;
import com.wss.bbb.e.mediation.interfaces.IMediationManager;
import com.wss.bbb.e.mediation.source.IBannerMaterial;
import com.wss.bbb.e.mediation.source.IInterstitialMaterial;
import com.wss.bbb.e.mediation.source.IRewardVideoMaterial;
import com.wss.bbb.e.mediation.source.LoadMaterialError;
import com.wss.bbb.e.mediation.source.RewardVideoError;
import com.wss.bbb.e.mediation.source.RewardVideoResult;
import com.wss.bbb.e.mediation.source.SceneInfo;
import com.wss.bbb.e.scene.WSSSceneSdk;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.wss.bbb.e.keeplive.KpConstants.CMD_KEY;
import static com.wss.bbb.e.keeplive.KpConstants.CMD_SUICIDE;
import static com.wss.bbb.e.keeplive.KpConstants.SP_KAL_CLOSE;

public class MainActivity extends AppCompatActivity {
    private LinearLayout mLinerLayoutContainer;
    private AtomicBoolean isRequestingRewardVideoAd = new AtomicBoolean(false);
    private AtomicBoolean isRequestingBannerAd = new AtomicBoolean(false);
    private AtomicBoolean isRequestingInterstitialAd = new AtomicBoolean(false);
    private IBannerMaterial mBannerMaterial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinerLayoutContainer = findViewById(R.id.ll_container);
        checkAndRequestPermission();
        WSSSceneSdk.onMainCreate(this);
        WSSKeepLive.init2(getApplicationContext(), null);
        //信息流
        addFeedAdButton();
        //激励视频
        addRewardVideoAdButton();
        //开屏
        addSplashAdButton();
        //banner
        addBannerAdButton();
        addDismissBannerAdButton();
        //draw视频
        addDrawVideoAdButton();
        //插屏（注意：全屏视频与插屏的请求方法相同，通过category区分。打底配置时，插屏的category是CATEGORY_INTERSTITIAL，全屏视频的category是CATEGORY_INTERSTITIAL_FULLSCREEN_VIDEO；
        // 走云控时，通过云控下发的code区分）
        addInterstitialAdButton();
        //终止保活
        addAliveStopButton();
    }

    private void addAliveStopButton() {
        Button button = addButton();
        button.setText("stop alive");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WssInstrumentation.close = true;
                /*关闭保活后，如果再想启动，请先调用如下两行代码，然后调用启动保活的方法
                CM.use(ISPUtils.class).putBoolean(getApplicationContext(), "kal_close", false);
                CM.use(IMultiProcessSPUtils.class).putString(getApplicationContext(), SP_KAL_CLOSE, "0");*/
                CM.use(ISPUtils.class).putBoolean(getApplicationContext(), "kal_close", true);
                CM.use(IMultiProcessSPUtils.class).putString(getApplicationContext(), SP_KAL_CLOSE, "1");


                int daemonPort = Integer.valueOf(CM.use(IMultiProcessSPUtils.class).getString(getApplicationContext(), AppProcessThread.SP_DAEMON_PORT, "0"));
                sendBySocket(daemonPort);
                int assist1Port = Integer.valueOf(CM.use(IMultiProcessSPUtils.class).getString(getApplicationContext(), AppProcessThread.SP_ASSIST1_PORT, "0"));
                sendBySocket(assist1Port);
                int assist2Port = Integer.valueOf(CM.use(IMultiProcessSPUtils.class).getString(getApplicationContext(), AppProcessThread.SP_ASSIST2_PORT, "0"));
                sendBySocket(assist2Port);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent3 = new Intent(MainActivity.this, WssNotifyResidentService.class);
                        intent3.putExtra(CMD_KEY, CMD_SUICIDE);
                        startService(intent3);

                        Intent intent = new Intent(MainActivity.this, WssDaemonService.class);
                        intent.putExtra(CMD_KEY, CMD_SUICIDE);
                        startService(intent);

                        Intent intent1 = new Intent(MainActivity.this, WssAssistService1.class);
                        intent1.putExtra(CMD_KEY, CMD_SUICIDE);
                        startService(intent1);

                        Intent intent2 = new Intent(MainActivity.this, WssAssistService2.class);
                        intent2.putExtra(CMD_KEY, CMD_SUICIDE);
                        startService(intent2);
                    }
                }, 200L);
            }
        });
    }


    private void addFeedAdButton() {
        Button button = addButton();
        button.setText("feed ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FeedListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addRewardVideoAdButton() {
        Button button = addButton();
        button.setText("reward video ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRewardVideoAd();
            }
        });
    }

    private void addSplashAdButton() {
        Button button = addButton();
        button.setText("splash ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addBannerAdButton() {
        Button button = addButton();
        button.setText("banner ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBannerAd();
            }
        });
    }

    private void addDismissBannerAdButton() {
        Button button = addButton();
        button.setText("dismiss banner ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBannerMaterial == null) {
                    Toast.makeText(MainActivity.this, "There is no banner showing", Toast.LENGTH_LONG).show();
                    return;
                }
                mBannerMaterial.dismiss();
                mBannerMaterial = null;
            }
        });
    }

    private void addDrawVideoAdButton() {
        Button button = addButton();
        button.setText("draw video ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DrawVideoAdActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addInterstitialAdButton() {
        Button button = addButton();
        button.setText("interstitial ad");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInterstitialAd();
            }
        });
    }


    private void showRewardVideoAd() {
        if (isRequestingRewardVideoAd.compareAndSet(false, true)) {
            IMediationManager adManager = WSSAdSdk.getAdManager();
            String pgtype = Constants.PGTYPE_REWARD_VIDEO;
            SceneInfo sceneInfo = new SceneInfo();
            sceneInfo.setPgtype(pgtype);
            adManager.loadRewardVideoMaterial(sceneInfo, new AdvMediationListener<IRewardVideoMaterial>() {
                @Override
                public boolean onLoad(IRewardVideoMaterial material) {
                    isRequestingRewardVideoAd.set(false);
                    //务必判断页面的状态
                    if (!Utils.isActivityAlive(MainActivity.this)) {
                        Toast.makeText(MainActivity.this, "MainActivity not alive!", Toast.LENGTH_LONG).show();
                        return false;//如果广告没有被消耗掉返回false
                    }
                    if (material == null) {
                        Toast.makeText(MainActivity.this, "no ad", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    Toast.makeText(MainActivity.this, "receive ad, ready to show", Toast.LENGTH_LONG).show();
                    material.show(MainActivity.this, new IRewardVideoListener() {
                        @Override
                        public void onComplete(RewardVideoResult result) {
                            Toast.makeText(MainActivity.this, "reward video onComplete()  result=" + result.isVerified(), Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(RewardVideoError error) {
                            Toast.makeText(MainActivity.this, "reward video onError()  code=" + error.getCode() + "  msg=" + error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                }

                @Override
                public void onError(LoadMaterialError error) {
                    isRequestingRewardVideoAd.set(false);
                    Toast.makeText(MainActivity.this, "MainActivity::onError()  code=" + error.getCode() + "  msg=" + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(MainActivity.this, "plz wait current request finish!", Toast.LENGTH_LONG).show();
        }
    }

    private void showBannerAd() {
        if (isRequestingBannerAd.compareAndSet(false, true)) {
            IMediationManager adManager = WSSAdSdk.getAdManager();
            String pgtype = Constants.PGTYPE_BANNER;
            SceneInfo sceneInfo = new SceneInfo();
            sceneInfo.setPgtype(pgtype);
            adManager.loadBannerMaterial(sceneInfo, new AdvMediationListener<IBannerMaterial>() {
                @Override
                public boolean onLoad(IBannerMaterial material) {
                    isRequestingBannerAd.set(false);
                    //务必判断页面的状态
                    if (!Utils.isActivityAlive(MainActivity.this)) {
                        Toast.makeText(MainActivity.this, "MainActivity not alive!", Toast.LENGTH_LONG).show();
                        return false;//如果广告没有被消耗掉返回false
                    }
                    if (material == null) {
                        Toast.makeText(MainActivity.this, "no ad", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    Toast.makeText(MainActivity.this, "receive ad, ready to show", Toast.LENGTH_LONG).show();
                    mBannerMaterial = material;
                    material.show(MainActivity.this, Gravity.BOTTOM, 0, Utils.dp2px(MainActivity.this, 20), true, new IMaterialInteractionListener() {
                        @Override
                        public void onAdShow() {
                            Toast.makeText(MainActivity.this, "Banner::onAdShow()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onAdClick() {
                            Toast.makeText(MainActivity.this, "Banner::onAdClick()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onCreativeButtonClick() {
                            Toast.makeText(MainActivity.this, "Banner::onCreativeButtonClick()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onAdvClose() {
                            Toast.makeText(MainActivity.this, "Banner::onAdvClose()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onDislikeSelect() {

                        }
                    });
                    return true;
                }

                @Override
                public void onError(LoadMaterialError error) {
                    isRequestingBannerAd.set(false);
                    Toast.makeText(MainActivity.this, "MainActivity::onError()  code=" + error.getCode() + "  msg=" + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(MainActivity.this, "plz wait current request finish!", Toast.LENGTH_LONG).show();
        }
    }


    private void showInterstitialAd() {
        if (isRequestingInterstitialAd.compareAndSet(false, true)) {
            IMediationManager adManager = WSSAdSdk.getAdManager();
            String pgtype = Constants.PGTYPE_INTERSTITIAL;
            SceneInfo sceneInfo = new SceneInfo();
            sceneInfo.setPgtype(pgtype);
            adManager.loadInterstitialMaterial(sceneInfo, new AdvMediationListener<IInterstitialMaterial>() {
                @Override
                public boolean onLoad(IInterstitialMaterial material) {
                    isRequestingInterstitialAd.set(false);
                    //务必判断页面的状态
                    if (!Utils.isActivityAlive(MainActivity.this)) {
                        Toast.makeText(MainActivity.this, "MainActivity not alive!", Toast.LENGTH_LONG).show();
                        return false;//如果广告没有被消耗掉返回false
                    }
                    if (material == null) {
                        Toast.makeText(MainActivity.this, "no ad", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    Toast.makeText(MainActivity.this, "receive ad, ready to show", Toast.LENGTH_LONG).show();
                    material.show(MainActivity.this, new IInterstitialListener() {
                        @Override
                        public void onAdShow() {
                            Toast.makeText(MainActivity.this, "InterstitialAd::onAdShow()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onAdClick() {
                            Toast.makeText(MainActivity.this, "InterstitialAd::onAdClick()", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onAdClose() {
                            Toast.makeText(MainActivity.this, "InterstitialAd::onAdClose()", Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                }

                @Override
                public void onError(LoadMaterialError error) {
                    isRequestingInterstitialAd.set(false);
                    Toast.makeText(MainActivity.this, "InterstitialAd::onError()  code=" + error.getCode() + "  msg=" + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(MainActivity.this, "plz wait current request finish!", Toast.LENGTH_LONG).show();
        }
    }

    private Button addButton() {
        Button button = new Button(MainActivity.this);
        button.setTextColor(Color.parseColor("#666666"));
        int height = Utils.dp2px(this, 45);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(MATCH_PARENT, height);
        int dp15 = Utils.dp2px(this, 15);
        lp.leftMargin = dp15;
        lp.topMargin = dp15;
        lp.rightMargin = dp15;
        lp.bottomMargin = dp15;
        mLinerLayoutContainer.addView(button, lp);
        return button;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkAndRequestPermission() {
        List<String> lackedPermission = new ArrayList<String>();
        if (!(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        // 权限都已经有了，那么直接调用SDK
        if (lackedPermission.size() == 0) {
            WSSSceneSdk.onGranted(this, true);
        } else {
            // 请求所缺少的权限，在onRequestPermissionsResult中再看是否获得权限，如果获得权限就可以调用SDK，否则不要调用SDK。
            String[] requestPermissions = new String[lackedPermission.size()];
            lackedPermission.toArray(requestPermissions);
            requestPermissions(requestPermissions, 1024);
        }
    }

    private boolean hasAllPermissionsGranted(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1024 && hasAllPermissionsGranted(grantResults)) {
            WSSSceneSdk.onGranted(this,true);
        } else {
            // 如果用户没有授权，那么应该说明意图，引导用户去设置里面授权。
            Toast.makeText(this, "应用缺少必要的权限！请点击\"权限\"，打开所需要的权限。", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        WSSSceneSdk.onMainDestroy(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
        WSSSceneSdk.onBackPressed(this);
    }

    private void sendBySocket(final int port) {
        Log.d("travis", "MainActivity::sendBySocket::port=" + port);
        if (port > 0) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Socket socket = null;
                    try {// 创建一个Socket对象，并指定服务端的IP及端口号
                        socket = new Socket("127.0.0.1", port);
                        /** 或创建一个报文，使用BufferedWriter写入,看你的需求 **/
                        String socketData = "terminate";
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                                socket.getOutputStream()));
                        writer.write(socketData.replace("\n", " ") + "\n");
                        writer.flush();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (socket != null) {
                            try {
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }).start();
        }
        /************************************************/

    }
}

