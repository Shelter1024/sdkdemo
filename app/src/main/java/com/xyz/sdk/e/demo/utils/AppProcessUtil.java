package com.xyz.sdk.e.demo.utils;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * des ：
 * created by ：wuchangbin
 * created on：2019/6/28
 */
public class AppProcessUtil {

    /**
     * 获取进程名称
     *
     * @param context
     * @return
     */
    public static String getCurrentProcessName(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses != null) {
                for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                    if (appProcess.pid == android.os.Process.myPid()) {
                        return appProcess.processName;
                    }
                }
            }
        }
        return context.getPackageName();
    }

    public static boolean isMainProcess(Context context) {
        String currentProcessName = getCurrentProcessName(context);
        return currentProcessName != null && currentProcessName.equals(context.getPackageName());
    }
}
