package com.xyz.sdk.e.demo;

import android.content.Context;

import com.wss.bbb.e.biz.config.IUrlsProvider;

public class ClientLinksProvider implements IUrlsProvider {
    public static final boolean TEST_SERVER = ClientConstants.TEST_SERVER;

    /*** 归因渠道接口 ** @return */
    @Override
    public String vtaInfoUrl(Context context) {
        return TEST_SERVER
                ? "http://test-urec-dtdb.mop.com/querydata/query/getUserData"
                : "https://urec-dtdb.mop.com/querydata/query/getUserData";
    }

    /*** 位置信息接口 ** @return */
    @Override
    public String locationInfoUrl(Context context) {
        return TEST_SERVER
                ? "http://test-adctrlpre-dtdb.mop.com/app-fix/adv/areaCode.data"
                : "https://adctrlpre-dtdb.mop.com/app-fix/adv/areaCode.data";
    }

    /*** 历史位置信息 ** @return */
    @Override
    public String historyLocationInfoUrl(Context context) {
        return TEST_SERVER
                ? "http://test-hispos-dtdb.mop.com/zt_userinfo/query/getUserSphereOfActivity"
                : "https://hispos-dtdb.mop.com/zt_userinfo/query/getUserSphereOfActivity";
    }

    /*** 序言泽 dsp 请求接口 ** @return */
    @Override
    public String dspRequestUrl(Context context) {
        return TEST_SERVER
                ? "http://test-nativematerial.tt.cn/sdknative/appmaterial"
                : "https://gproverbnativesdk.tt.cn/sdknative/appmaterial";
    }

    /*** sdk 通用上报接口 ** @return */
    @Override
    public String sdkCommonReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdkreport"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdkreport";
    }

    /*** dsp 用户画像接口 ** @return */
    @Override
    public String hbaseLinkUrl(Context context) {
        return TEST_SERVER
                ? "http://test-nativematerial.tt.cn/appnative/hbaselink"
                : "https://answeruserportrait.tt.cn/infonative/hbaselink";
    }

    /*** sdk 请求上报接口 ** @return */
    @Override
    public String sdkRequestReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdknewrequest"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdknewrequest";
    }

    /**
     * sdk 返回上报接口 ** @return
     */
    @Override
    public String sdkReturnReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdkreturn"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdkreturn";
    }

    /*** sdk 展示上报接口 ** @return */
    @Override
    public String sdkShowReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdkshow"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdkshow";
    }

    /*** sdk 点击上报接口 ** @return */
    @Override
    public String sdkClickReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdkclick"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdkclick";
    }

    /*** 广告云控接口 ** @return */
    @Override
    public String pollingUrl(Context context) {
        return TEST_SERVER
                ? "http://test-adctrlbsc-dtdb.mop.com/advertisement-cloud-api/data/adv.data"
                : "https://adctrlbsc-dtdb.mop.com/advertisement-cloud-api/data/adv.data";
    }

    /*** 广告云控前置接口 ** @return */
    @Override
    public String extInfoUrl(Context context) {
        return TEST_SERVER
                ? "http://test-adctrlpre-dtdb.mop.com/app-fix/adv/advFix.data"
                : "https://adctrlpre-dtdb.mop.com/app-fix/adv/advFix.data";
    }

    /*** 广告云控前置接口 ** @return */
    @Override
    public String appListUrl(Context context) {
        return TEST_SERVER
                ? "http://test-nativematerial.tt.cn/applist/applist.report"
                : "https://exportapplist.eastdaymedia.cn/applist/applist.report";
    }

    @Override
    public String externalLogUrl(Context context) {
        //return "https://exterlog-inside.tt.cn/apppubliclogs/exterlog";
        return null;
    }

    @Override
    public String externalCtrlUrl(Context context) {
        //return "https://adctrlext-inside.tt.cn/external-adv-cloud-api/config/adv.config";
        return null;
    }


    /*** 广告触发上报 ** @return */
    @Override
    public String triggerReportUrl(Context context) {
        return TEST_SERVER
                ? "http://advsdkreport-def.tt.cn/apppubliclogs/sdktrigger"
                : "https://advsdkreport-def.tt.cn/apppubliclogs/sdktrigger";
    }

    /*** 激励视频播放完成上报 ** @return */
    @Override
    public String finalPlayUrl(Context context) {
        return TEST_SERVER
                ? "http://advsdkreport-def.tt.cn/apppubliclogs/sdkfinalplay"
                : "https://advsdkreport-def.tt.cn/apppubliclogs/sdkfinalplay";
    }

    @Override
    public String newLocationInfoUrl(Context context) {
        return null;
    }

    @Override
    public String installTimeUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-def.tt.cn/apppubliclogs/sdkgettime"
                : "https://advsdkreport-def.tt.cn/apppubliclogs/sdkgettime";
    }

    @Override
    public String sdkBidReportUrl(Context context) {
        return TEST_SERVER
                ? "http://test-advsdkreport-dtdb.mop.com/apppubliclogs/sdkbigding"
                : "https://advsdkreport-dtdb.mop.com/apppubliclogs/sdkbigding";
    }
}

