package com.xyz.sdk.e.demo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.wss.bbb.e.WSSConstants;
import com.wss.bbb.e.display.IMaterialView;
import com.wss.bbb.e.display.MaterialViewSpec;
import com.wss.bbb.e.display.WssRenderUtils;
import com.wss.bbb.e.mediation.WSSMediationManager;
import com.wss.bbb.e.mediation.api.IMaterialInteractionListener;
import com.wss.bbb.e.mediation.api.AdvMediationListener;
import com.wss.bbb.e.mediation.source.IDrawVideoMaterial;
import com.wss.bbb.e.mediation.source.LoadMaterialError;
import com.wss.bbb.e.mediation.source.SceneInfo;

import java.lang.ref.WeakReference;

public class DrawVideoAdActivity extends AppCompatActivity {
    private static final String TAG = DrawVideoAdActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        FrameLayout contaier = findViewById(R.id.container);
        String pgtype = Constants.PGTYPE_DRAW_VIDEO;
        SceneInfo sceneInfo = new SceneInfo();
        sceneInfo.setPgtype(pgtype);
        WeakRefMediationListener weakRefMediationListener = new WeakRefMediationListener(DrawVideoAdActivity.this, contaier);
        WSSMediationManager.getInstance().loadDrawVideoMaterial(sceneInfo, weakRefMediationListener);
    }

    /**
     * 避免出现内存泄露的问题
     */
    private static class WeakRefMediationListener implements AdvMediationListener<IDrawVideoMaterial> {
        private WeakReference<Activity> activityWeakReference;
        private WeakReference<FrameLayout> containerRef;

        public WeakRefMediationListener(Activity activity, FrameLayout container) {
            this.activityWeakReference = new WeakReference<>(activity);
            this.containerRef = new WeakReference<>(container);
        }

        @Override
        public boolean onLoad(IDrawVideoMaterial material) {
            Toast.makeText(App.getContext(), "DrawVideoAd::onLoad()", Toast.LENGTH_LONG).show();

            //todo 判断activity的状态
            //todo  判断container的状态
            final Activity activity = activityWeakReference.get();
            FrameLayout contaier = containerRef.get();
            if (activity == null || contaier == null) {
                Toast.makeText(App.getContext(), "activity not alive!", Toast.LENGTH_LONG).show();
                return false;
            }
            IMaterialView materialView = new DrawVideoMaterialView(activity);
            contaier.addView(materialView.getRoot());
            MaterialViewSpec materialViewSpec = new MaterialViewSpec();
            materialViewSpec.context = activity;
            materialViewSpec.mSupportStyles = new int[]{WSSConstants.FEED_STYLE_DRAW_VIDEO};
            WssRenderUtils.render(materialView, material, materialViewSpec, new IMaterialInteractionListener() {
                @Override
                public void onAdShow() {
                    Toast.makeText(activity, "DrawVideoAd::onAdShow()", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onAdClick() {
                    Toast.makeText(activity, "DrawVideoAd::onAdClick()", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCreativeButtonClick() {
                    Toast.makeText(activity, "DrawVideoAd::onCreativeButtonClick()", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onAdvClose() {
                    Toast.makeText(activity, "DrawVideoAd::onAdvClose()", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onDislikeSelect() {

                }
            });
            return false;
        }

        @Override
        public void onError(LoadMaterialError error) {
            Log.d(TAG, "DrawVideoAd::onError()  code=" + error.getCode() + "  message=" + error.getMessage());
        }
    }
}
