package com.xyz.sdk.e.demo;

import android.app.Application;

import com.wss.bbb.e.ICusParams;
import com.wss.bbb.e.WSSAdConfig;
import com.wss.bbb.e.WSSAdSdk;
import com.wss.bbb.e.biz.config.IUrlsProvider;
import com.wss.bbb.e.mediation.api.IDefaultConfigProvider;

public class AdSdkInitializer {
    public static void init(Application app) {
        //初始化广告中台 start
        ICusParams customParams = new ClientCustomParams();
        IDefaultConfigProvider configProvider = new ClientDefaultConfigProvider();
        IUrlsProvider linksProvider = new ClientLinksProvider();
        WSSAdConfig adConfig = new WSSAdConfig.Builder()
                .setDefaultConfigProvider(configProvider)
                .setCustomParams(customParams)
                .setLinksProvider(linksProvider)
                .setTestServer(ClientConstants.TEST_SERVER)
                .setDebug(ClientConstants.DEBUG)
                .build();
        WSSAdSdk.init(app, adConfig);
        //初始化广告中台 end
    }
}
