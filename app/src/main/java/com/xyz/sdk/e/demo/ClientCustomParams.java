package com.xyz.sdk.e.demo;


import com.wss.bbb.e.ICusParams;

public class ClientCustomParams implements ICusParams {

    public ClientCustomParams() {
    }

    @Override
    public String accId() {
        return null;
    }

    @Override
    public String muid() {
        return null;
    }

    @Override
    public String appTypeId() {
        return ClientConstants.APP_TYPE_ID;
    }

    @Override
    public String appQid() {
        return null;
    }

    @Override
    public String cleanAppQid() {
        return null;
    }

    @Override
    public String isTourist() {
        return null;
    }

    @Override
    public String appSmallVerInt() {
        return null;
    }

    @Override
    public String appSmallVer() {
        return null;
    }

    @Override
    public String aaid() {
        return "ef8d6c4e5342546918db118defb2b95d";//MiitHelper.aaid;
    }

    @Override
    public String oaid() {
        return "7fdf7ffb-f5ff-80a7-7ddb-fbaffde6dbc8";//MiitHelper.oaid;
    }

    @Override
    public String softType() {
        return null;
    }

    @Override
    public String softName() {
        return null;
    }

    @Override
    public String userflag() {
        return null;
    }

    @Override
    public String userinfo() {
        return null;
    }

    @Override
    public boolean lowGps() {
        return false;
    }

    /**
     * 兼容华为手机过审策略
     *
     * @return
     */
    @Override
    public boolean canUseMacAddress() {
        return true;
    }

    @Override
    public boolean isUseCacheFirst(String s, String s1) {
        return false;
    }

    @Override
    public long cacheTimeForSafeExposure(String s) {
        return 0;
    }

    @Override
    public boolean useClientLocation() {
        return false;
    }

    @Override
    public float lat() {
        return 0;
    }

    @Override
    public float lng() {
        return 0;
    }

    @Override
    public long lbsTime() {
        return 0;
    }
}
