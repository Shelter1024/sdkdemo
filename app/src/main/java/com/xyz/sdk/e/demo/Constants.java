package com.xyz.sdk.e.demo;

public class Constants {
    public static final String PGTYPE_FEED = "feed ad";
    public static final String PGTYPE_REWARD_VIDEO = "reward video ad";
    public static final String PGTYPE_SPLASH = "splash ad";
    public static final String PGTYPE_BANNER = "banner ad";
    public static final String PGTYPE_DRAW_VIDEO = "draw video ad";
    public static final String PGTYPE_INTERSTITIAL = "interstitial ad";

}
