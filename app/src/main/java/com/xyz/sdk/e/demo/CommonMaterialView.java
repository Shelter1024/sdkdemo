package com.xyz.sdk.e.demo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.wss.bbb.e.display.BaseMaterialView;

public class CommonMaterialView extends BaseMaterialView {

    public CommonMaterialView(Context context) {
        super(context);
    }

    public CommonMaterialView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CommonMaterialView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getLayoutId() {
        return R.layout.adv_material_view_common;
    }
}
