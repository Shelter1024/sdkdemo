package com.xyz.sdk.e.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wss.bbb.e.WSSAdSdk;
import com.xyz.sdk.e.demo.utils.Utils;
import com.wss.bbb.e.display.MaterialViewSpec;
import com.wss.bbb.e.display.WssRenderUtils;
import com.wss.bbb.e.mediation.api.IMaterialInteractionListener;
import com.wss.bbb.e.mediation.api.AdvMediationListener;
import com.wss.bbb.e.mediation.interfaces.IMediationManager;
import com.wss.bbb.e.mediation.source.IEmbeddedMaterial;
import com.wss.bbb.e.mediation.source.LoadMaterialError;
import com.wss.bbb.e.mediation.source.SceneInfo;

import static com.wss.bbb.e.WSSConstants.FEED_STYLE_LARGE;
import static com.wss.bbb.e.WSSConstants.FEED_STYLE_NONE;
import static com.wss.bbb.e.WSSConstants.FEED_STYLE_TEMPLATE;
import static com.wss.bbb.e.WSSConstants.FEED_STYLE_VIDEO;

public class FeedListActivity extends AppCompatActivity {
    private static final String TAG = FeedListActivity.class.getSimpleName();
    private CommonMaterialView mMaterialView;
    private Button mButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedlist);
        mButton = findViewById(R.id.button);
        mMaterialView = findViewById(R.id.material_view);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFeedAd();
            }
        });
    }

    private void showFeedAd() {
        IMediationManager adManager = WSSAdSdk.getAdManager();
        String pgtype = Constants.PGTYPE_FEED;
        SceneInfo sceneInfo = new SceneInfo();
        sceneInfo.setPgtype(pgtype);
        adManager.loadEmbeddedMaterial(sceneInfo, new AdvMediationListener<IEmbeddedMaterial>() {
            @Override
            public boolean onLoad(IEmbeddedMaterial material) {
                //务必判断页面的状态
                if (!Utils.isActivityAlive(FeedListActivity.this)) {
                    Toast.makeText(FeedListActivity.this, "FeedListActivity not alive!", Toast.LENGTH_LONG).show();
                    return false;//如果广告没有被消耗掉返回false
                }
                if (material == null) {
                    Toast.makeText(FeedListActivity.this, "no ad", Toast.LENGTH_LONG).show();
                    return false;
                }

                MaterialViewSpec materialViewSpec = new MaterialViewSpec();
                materialViewSpec.context = FeedListActivity.this;//必须是Activity,如果在dialog上展示广告需要对MaterialViewSpec.dialog赋值
                materialViewSpec.mSupportStyles = new int[]{FEED_STYLE_TEMPLATE, FEED_STYLE_VIDEO, FEED_STYLE_LARGE, FEED_STYLE_NONE};
                WssRenderUtils.render(mMaterialView, material, materialViewSpec, new IMaterialInteractionListener() {
                    @Override
                    public void onAdShow() {
                        Toast.makeText(FeedListActivity.this, "FeedListActivity::onAdShow()", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onAdClick() {
                        Toast.makeText(FeedListActivity.this, "FeedListActivity::onAdClick()", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCreativeButtonClick() {
                        Toast.makeText(FeedListActivity.this, "FeedListActivity::onCreativeButtonClick()", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onAdvClose() {
                        Toast.makeText(FeedListActivity.this, "FeedListActivity::onAdvClose()", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onDislikeSelect() {

                    }
                });
                return true;//如果广告被消耗掉返回true
            }

            @Override
            public void onError(LoadMaterialError error) {
                Toast.makeText(FeedListActivity.this, "FeedListActivity::onError()  code=" + error.getCode() + "  msg=" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
